const RippleAPI = require('ripple-lib').RippleAPI;

const api = new RippleAPI({
    server: 'wss://s1.ripple.com' // Public rippled server
});

const RippleAPIMethods = {
    getBalance: async (walletAddress) => {
        await api.connect();
        console.log('Connected to the api');
        const { xrpBalance } = await api.getAccountInfo(walletAddress);
        console.log(`Balance: ${xrpBalance}`);
        await api.disconnect();
        console.log('Done and disconnected');
        return xrpBalance;
    }
};

module.exports = {
    RippleAPIMethods
};