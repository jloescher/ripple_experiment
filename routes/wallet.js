require('dotenv').config();
const express = require('express');
const router = express.Router();
const { RippleAPIMethods } = require('../models/RippleAPI');
const db = require('../config/db');

/* GET wallets. */
const SELECT_SQL = `SELECT wallet_address FROM wallets`;
router.get('/', function(req, res, next) {

    db.connection.query(SELECT_SQL, async function (err, results) {
        if (err) throw err;
        const wallets = await Promise.all(
            results.map(async (result) => ({
                "wallet_address": result.wallet_address,
                "balance": await RippleAPIMethods.getBalance(result.wallet_address)
            }))
        );
        res.render('pages/wallet/showWallets', {
            title: 'XRP Wallet Tracker',
            wallets
        });
    });
});

router.get('/add', function (req, res) {
    res.render('pages/wallet/addWallet', { title: 'XRP Wallet Tracker' });
});

const INSERT_SQL = `INSERT INTO wallets ( user_id, wallet_address ) VALUES ( ?, ? )`;
router.post('/add', function (req, res) {
    const walletAddress = req.body.wallet_address;
    const user_id = 1; // TODO: Create user functionality and have this capture the ID of the user that is logged in.
    db.connection.query(INSERT_SQL, [user_id, walletAddress], function (err) {
        if (err) {
            console.log("There was an error storing to the DB: " + err.stack);
        } else {
            console.log("Wallet address successfully stored.");
        }
    });
    res.redirect('/wallet');
});

module.exports = router;